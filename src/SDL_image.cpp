#include "SDL2PP/SDL_image.h"
#include "SDL2PP/SDL_render.h"
#include <stdexcept>

namespace SDL
{
    Texture::Texture(Renderer &renderer, const std::string &file)
        : wrapped{IMG_LoadTexture(renderer.get_wrapped(), file.c_str())}
    {
        if (wrapped == nullptr)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }
}
