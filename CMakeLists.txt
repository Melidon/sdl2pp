cmake_minimum_required(VERSION 3.13...3.19 FATAL_ERROR)
project(SDL2PP VERSION 1.0.0 LANGUAGES CXX)

# Needed so that cmake uses our find modules.
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# Find system dependencies.
find_package(SDL2 REQUIRED)
find_package(SDL2_net REQUIRED)
find_package(SDL2_mixer REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(SDL2_gfx REQUIRED)
find_package(SDL2_ttf REQUIRED)

# Create target and set properties.
set(SOURCE_FILES
    ./src/SDL_events.cpp
    ./src/SDL_image.cpp
    ./src/SDL_render.cpp
    ./src/SDL_timer.cpp
    ./src/SDL_video.cpp
    ./src/SDL.cpp
    )

add_library(SDL2PP ${SOURCE_FILES})

# Add an alias so that library can be used inside the build tree, e.g. when testing.
add_library(SDL2PP::SDL2PP ALIAS SDL2PP)

# Set target properties.
target_include_directories(SDL2PP
    PUBLIC
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
)

target_compile_options(SDL2PP
    PRIVATE
        -Werror -Wall -Wextra
)

target_link_libraries(SDL2PP
    PUBLIC
        SDL2::Main SDL2::Net SDL2::Mixer SDL2::Image SDL2::TTF SDL2::GFX
)
