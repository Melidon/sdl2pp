#pragma once

#include <SDL2/SDL_render.h>
#include "SDL_video.h"

namespace SDL
{
    class Texture;

    using Rect = SDL_Rect;

    class Renderer
    {
    public:
        enum class Flags : Uint32
        {
            SOFTWARE = SDL_RENDERER_SOFTWARE,
            ACCELERATED = SDL_RENDERER_ACCELERATED,
            PRESENTVSYNC = SDL_RENDERER_PRESENTVSYNC,
            TARGETTEXTURE = SDL_RENDERER_TARGETTEXTURE
        };

    private:
        SDL_Renderer *wrapped;

    public:
        explicit Renderer(Window &window, int index, Flags flags);
        virtual ~Renderer();
        Renderer(const Renderer &other) = delete;
        Renderer(Renderer &&other) noexcept;
        Renderer &operator=(const Renderer &other) = delete;
        Renderer &operator=(Renderer &&other) noexcept;

        bool operator==(const Renderer &other) const;
        bool operator!=(const Renderer &other) const;

        SDL_Renderer *get_wrapped(void);

        void clear(void);
        void copy(Texture &texture, const Rect &srcrect, const Rect &dstrect);
        void present(void);
    };

    class Texture
    {
    private:
        SDL_Texture *wrapped;

    public:
        explicit Texture(Renderer &renderer, Uint32 format, int access, int w, int h);
        explicit Texture(Renderer &renderer, const std::string &file);
        virtual ~Texture();
        Texture(const Texture &other) = delete;
        Texture(Texture &&other) noexcept;
        Texture &operator=(const Texture &other) = delete;
        Texture &operator=(Texture &&other) noexcept;

        bool operator==(const Texture &other) const;
        bool operator!=(const Texture &other) const;

        SDL_Texture *get_wrapped(void);
    };
}
