#pragma once

#include <SDL2/SDL.h>

//#include "SDL_main.h"
//#include "SDL_stdinc.h"
//#include "SDL_assert.h"
//#include "SDL_atomic.h"
//#include "SDL_audio.h"
//#include "SDL_clipboard.h"
//#include "SDL_cpuinfo.h"
//#include "SDL_endian.h"
//#include "SDL_error.h"
#include "SDL_events.h"
//#include "SDL_filesystem.h"
//#include "SDL_gamecontroller.h"
//#include "SDL_haptic.h"
//#include "SDL_hidapi.h"
//#include "SDL_hints.h"
//#include "SDL_joystick.h"
//#include "SDL_loadso.h"
//#include "SDL_log.h"
//#include "SDL_messagebox.h"
//#include "SDL_metal.h"
//#include "SDL_mutex.h"
//#include "SDL_power.h"
#include "SDL_render.h"
//#include "SDL_rwops.h"
//#include "SDL_sensor.h"
//#include "SDL_shape.h"
//#include "SDL_system.h"
//#include "SDL_thread.h"
#include "SDL_timer.h"
//#include "SDL_version.h"
#include "SDL_video.h"
//#include "SDL_locale.h"
//#include "SDL_misc.h"

namespace SDL
{
    enum class InitFlags : Uint32
    {
        TIMER = SDL_INIT_TIMER,
        AUDIO = SDL_INIT_AUDIO,
        VIDEO = SDL_INIT_VIDEO,
        JOYSTICK = SDL_INIT_JOYSTICK,
        HAPTIC = SDL_INIT_HAPTIC,
        GAMECONTROLLER = SDL_INIT_GAMECONTROLLER,
        EVENTS = SDL_INIT_EVENTS,
        SENSOR = SDL_INIT_SENSOR,
        NOPARACHUTE = SDL_INIT_NOPARACHUTE,
        EVERYTHING = SDL_INIT_EVERYTHING,
    };

    void init(InitFlags flags);
    void quit(void);
}
